﻿using UnityEngine;

/// <summary>
/// This class moves enemyballs in survival mode to the bottom of the screen.
/// The game is over if any enemyball reach to the botttom.
/// </summary>
public class EnemyBallController : MonoBehaviour
{
    private readonly float destroyThreshold = -6.5f; //if position is passed this value, the game is over.
    private float speed; //movement speed (the faster, the harder)

    private void Start()
    {
        //set a random speed for each enemyball
        speed = Random.Range(0.6f, 2.0f);
    }

    private void Update()
    {
        //move the enemyball down
        var z = Time.deltaTime * GameController.GameState.moveSpeed * speed;
        transform.position -= new Vector3(0, 0, z);
        //check for possible gameover
        if (transform.position.z < destroyThreshold)
        {
            GameController.GameState.gameOver = true;
            Destroy(gameObject);
        }
    }
}