﻿using UnityEngine;

/// <summary>
/// Main Player manager class.
/// We check all player collisions here.
/// We also calculate the score in this class.
/// </summary>
public class PlayerManager : MonoBehaviour
{
    public static int playerScore; //player score
    public static float sessionLength; //session length
    public GameObject scoreTextDynamic; //gameobject which shows the score on screen
    
    private void Awake()
    {
        playerScore = 0;
        sessionLength = 0;

        //Disable screen dimming on mobile devices
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //Playe the game with a fixed framerate in all platforms
        Application.targetFrameRate = 60;
    }

    private void Update()
    {
        if (GameController.GameState.gameOver)
        {
            enabled = false;
            return;
        }

        CalculateScore();
    }

    /// <summary>
    /// Collision detection and management
    /// </summary>
    /// <param name="c"></param>
    private void OnCollisionEnter(Collision c)
    {
        //collision with mazes and enemyballs leads to a sudden gameover

        var gameObject = c.gameObject;
        if (gameObject.CompareTag("Maze"))
        {
            print("Game Over");
            GameController.GameState.gameOver = true;
        }

        if (gameObject.CompareTag("enemyBall"))
        {
            Destroy(gameObject);
        }
    }


    /// <summary>
    /// calculate player's score
    /// Score is a combination of gameplay duration (while player is still alive) 
    /// and a multiplier for the current level.
    /// </summary>
    private void CalculateScore()
    {
        if (!PauseManager.isPaused)
        {
            sessionLength += Time.deltaTime;
            playerScore += (int) (GameController.GameState.currentLevel * Mathf.Log(GameController.GameState.currentLevel + 1, 2));
            scoreTextDynamic.GetComponent<TextMesh>().text = playerScore.ToString();
        }
    }
}