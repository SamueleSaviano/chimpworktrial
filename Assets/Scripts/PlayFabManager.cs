﻿using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class PlayFabManager : MonoBehaviour
{
    public static PlayFabManager Instance;
    public static bool Logged;
    public static bool CanTrack;

    private Dictionary<string, object> _eventBody = new Dictionary<string, object>();

    [SerializeField]
    private EventsData _eventsData;

    private string _deviceID => SystemInfo.deviceUniqueIdentifier;    

    private void Awake()
    {
        if (PlayFabManager.Instance == null)
        {
            PlayFabManager.Instance = this;
            GameObject.DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        this.Login();
    }

    private void Login()
    {
        if (!PlayFabManager.Logged)
        {
            LoginWithCustomIDRequest _request = new LoginWithCustomIDRequest
            {
                CustomId = this._deviceID,
                CreateAccount = true
            };

            PlayFabClientAPI.LoginWithCustomID(_request, this.OnLoginSuccess, this.OnLoginFailure);
        }
    }

    private void OnLoginSuccess(LoginResult p_result)
    {
        Debug.Log($"<color=green>PlayFab Login Completed!</color> <color=yellow>UserID = {p_result.PlayFabId}</color>");

        PlayFabManager.Logged = true;
        PlayFabManager.CanTrack = true;
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogError($"<color=red>PlayFab Login Failed</color>\n<color=magenta>{error.GenerateErrorReport()}</color>");

        PlayFabManager.Logged = false;
        PlayFabManager.CanTrack = false;
    }

    public void SendPointsEvent(int score, string type)
    {
        this.SendEvent(EventType.PointScored, new object[] { score, type });
    }

    public void SendSessionEvent(int seconds)
    {
        this.SendEvent(EventType.SessionLength, new object[] { seconds });
    }

    public void SendEvent(EventType eventType, object[] values)
    {
        if (PlayFabManager.CanTrack)
        {
            this._eventBody.Clear();

            for (int i = 0; i < values.Length; i++)
            {
                this._eventBody.Add(this._eventsData.EventProperty(eventType, i), values[i]);
            }

            PlayFabClientAPI.WritePlayerEvent(new WriteClientPlayerEventRequest()
            {
                Body = this._eventBody,
                EventName = this._eventsData.EventName(eventType)
            },
            result => Debug.LogFormat($"<color=green>Event <color=yellow>{this._eventsData.EventName(eventType)}</color> Sent</color>"),
            error => Debug.LogErrorFormat($"<color=red>Event Error:</color> <color=magenta>{error.GenerateErrorReport()}</color>"));
        }
    }
}
