﻿using UnityEngine;

public static class MonoBehaviourExtensions
{
    /// <summary>
    /// Play audio clip
    /// </summary>
    /// <param name="script"></param>
    /// <param name="sfx"></param>
    public static void PlaySfx(this MonoBehaviour script, AudioClip sfx)
    {
        var audioSource = script.GetComponent<AudioSource>();

        audioSource.clip = sfx;
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }

    public static int Floor(this float value)
    {
        return Mathf.FloorToInt(value);
    }
}