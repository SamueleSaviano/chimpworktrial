﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GameState.asset", menuName = "Create GameState Object")]
[Serializable]
public class GameStateObject : ScriptableObject
{
    public GameMode gameMode;

    public int currentLevel = 1; //Start from easy settings (1 = very easy ---> 10 = very hard)
    public float levelPassedTime; //passed time since we started the game
    public float levelStartTime; //time of starting the game

    //Difficulty variables
    public float moveSpeed; //Global speed of moving items (mazes)
    public float cloneInterval; //clone maze and enenmyball every N seconds

    public bool gameOver;
    public bool gameOverFlag; //Run the gameover sequence just once

    //maze & enemyball creation flag
    public bool createMaze; //can we clone a new maze?
    public bool createEnemyBall; //can we clone a new enemyball?

    public void Reset(GameMode gameMode)
    {
        this.gameMode = gameMode;

        currentLevel = 1;
        levelPassedTime = 0;
        levelStartTime = 0;

        createMaze = true; //allow maze creation
        createEnemyBall = true; //allow enemyball creation

        moveSpeed = 1.2f;
        cloneInterval = 1.0f;
        gameOver = false;
        gameOverFlag = false;
    }
}