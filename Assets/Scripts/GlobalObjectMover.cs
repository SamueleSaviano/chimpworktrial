﻿using UnityEngine;

/// <summary>
/// This class moves the mazes down and destroys them when they pass a certain threshold
/// </summary>
public class GlobalObjectMover : MonoBehaviour
{
    [Range(1.5f, 2.5f)]
    public float speed = 2.0f; //movement speed
    private readonly float destroyThreshold = -10.0f; //destory passed mazes to free up the memory

    private void Update()
    {
        //Scroll down the maze objects
        var z = Time.deltaTime * GameController.GameState.moveSpeed * speed;
        transform.position -= new Vector3(0, 0, z);
        
        //Destroy it if it's out of screen view
        if (transform.position.z < destroyThreshold)
        {
            Destroy(gameObject);
        }
    }
}