﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum EventType
{
    SessionLength,
    PointScored
}

[CreateAssetMenu(fileName = "EventsData", menuName = "Data/Events Data")]
public class EventsData : ScriptableObject
{
    [Serializable]
    public struct EventData
    {
        [SerializeField]
        private EventType _type;
        [SerializeField]
        private string _name;
        [SerializeField]
        private string[] _properties;

        public EventType type => this._type;
        public string name => this._name;
        public string[] properties => this._properties;
    }

    [SerializeField]
    private List<EventData> _eventDatas = new List<EventData>();

    public EventData GetData(EventType type)
    {
        return this._eventDatas.Find(_x => _x.type == type);
    }

    public string EventName(EventType type)
    {
        return this.GetData(type).name;
    }

    public string EventProperty(EventType type, int index)
    {
        return this.GetData(type).properties[index];
    }
}
