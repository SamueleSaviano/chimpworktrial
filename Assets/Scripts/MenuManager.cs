﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Main Menu Controller.
/// This class handles user clicks on menu button, and also fetch and shows user saved scores on screen.
/// </summary>
public class MenuManager : MonoBehaviour
{
    //reference to gameObjects
    public GameObject bestScoreText;
    public GameObject lastScoreText;

    public AudioClip menuTap; //sfx for touch on menu buttons

    private int bestScore; //best saved score
    private readonly float buttonAnimationSpeed = 9.0f; //button scale animation speed

    private bool canTap; //are we allowed to click on buttons? (prevents double touch)

    /// <summary>
    /// Process user inputs
    /// </summary>
    private RaycastHit hitInfo;
    private int lastScore; //score of the last play
    private Ray ray;

    private void Awake()
    {
        canTap = true; //player can tap on buttons

        bestScore = PlayerPrefs.GetInt("bestScore");
        bestScoreText.GetComponent<TextMesh>().text = bestScore.ToString();

        lastScore = PlayerPrefs.GetInt("lastScore");
        lastScoreText.GetComponent<TextMesh>().text = lastScore.ToString();
    }

    private void Start()
    {
        //prevent screenDim in handheld devices
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private void Update()
    {
        if (canTap)
        {
            StartCoroutine(TapManager());
        }
    }

    private IEnumerator TapManager()
    {
        //Mouse of touch?
        if (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended)
        {
            ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        }
        else
        {
            yield break;
        }

        if (Physics.Raycast(ray, out hitInfo))
        {
            var objectHit = hitInfo.transform.gameObject;
            switch (objectHit.name)
            {
                case "Btn-Mode-01":
                    canTap = false;
                    this.PlaySfx(menuTap);
                    PlayerPrefs.SetInt("GameMode", 0);
                    StartCoroutine(AnimateButton(objectHit));
                    yield return new WaitForSeconds(1.0f);
                    SceneManager.LoadScene("Game");
                    break;

                case "Btn-Mode-02":
                    canTap = false;
                    this.PlaySfx(menuTap);
                    PlayerPrefs.SetInt("GameMode", 1);
                    StartCoroutine(AnimateButton(objectHit));
                    yield return new WaitForSeconds(1.0f);
                    SceneManager.LoadScene("Game");
                    break;

                case "btnExit":
                    canTap = false;
                    this.PlaySfx(menuTap);
                    StartCoroutine(AnimateButton(objectHit));
                    yield return new WaitForSeconds(1.0f);
                    Application.Quit();
                    break;
            }
        }
    }

    /// <summary>
    /// Animate button by modifying it's scales
    /// </summary>
    /// <param name="btn"></param>
    /// <returns></returns>
    private IEnumerator AnimateButton(GameObject btn)
    {
        var startingScale = btn.transform.localScale;
        var destinationScale = startingScale * 1.1f;
        var t = 0.0f;
        while (t <= 1.0f)
        {
            t += Time.deltaTime * buttonAnimationSpeed;
            btn.transform.localScale = new Vector3(Mathf.SmoothStep(startingScale.x, destinationScale.x, t),
                btn.transform.localScale.y,
                Mathf.SmoothStep(startingScale.z, destinationScale.z, t));
            yield return 0;
        }

        var r = 0.0f;
        if (btn.transform.localScale.x >= destinationScale.x)
        {
            while (r <= 1.0f)
            {
                r += Time.deltaTime * buttonAnimationSpeed;
                btn.transform.localScale = new Vector3(Mathf.SmoothStep(destinationScale.x, startingScale.x, r),
                    btn.transform.localScale.y,
                    Mathf.SmoothStep(destinationScale.z, startingScale.z, r));
                yield return 0;
            }
        }

        if (r >= 1)
        {
            canTap = true;
        }
    }
}