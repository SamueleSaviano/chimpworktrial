﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Gameover manager class.
/// </summary>
public class GameoverManager : MonoBehaviour
{
    public GameObject scoreText; //gameobject which shows the score on screen
    public GameObject bestScoreText; //gameobject which shows the best saved score on screen

    public AudioClip menuTap; //touch sound
    private readonly float buttonAnimationSpeed = 9.0f; //button scale animation speed

    private bool canTap; //prevents double click on buttons

    /// <summary>
    /// Manage user taps on gameover buttons
    /// </summary>
    private RaycastHit hitInfo;
    private Ray ray;

    private void Awake()
    {
        canTap = true;
        //Set the new score on the screen
        scoreText.GetComponent<TextMesh>().text = PlayerManager.playerScore.ToString();
        bestScoreText.GetComponent<TextMesh>().text = PlayerPrefs.GetInt("bestScore").ToString();
    }

    private void Update()
    {
        if (canTap)
        {
            StartCoroutine(TapManager());
        }
    }

    private IEnumerator TapManager()
    {
        //Mouse of touch?
        if (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended)
        {
            ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        }
        else
        {
            yield break;
        }

        if (Physics.Raycast(ray, out hitInfo))
        {
            var objectHit = hitInfo.transform.gameObject;
            switch (objectHit.name)
            {
                case "retryButton":
                    this.PlaySfx(menuTap); //play audioclip
                    SaveScore(); //save players best and last score
                    StartCoroutine(AnimateButton(objectHit)); //animate the button
                    yield return new WaitForSeconds(0.4f); //Wait
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name); //Load next level
                    break;
                case "menuButton":
                    this.PlaySfx(menuTap);
                    SaveScore();
                    StartCoroutine(AnimateButton(objectHit));
                    yield return new WaitForSeconds(1.0f);
                    SceneManager.LoadScene("Menu");
                    break;
            }
        }
    }

    /// <summary>
    /// Save player score
    /// </summary>
    private void SaveScore()
    {
        //immediately save the last score
        PlayerPrefs.SetInt("lastScore", PlayerManager.playerScore);
        //check if this new score is higher than saved bestScore.
        //if so, save this new score into playerPrefs. otherwise keep the last bestScore intact.
        int lastBestScore;
        lastBestScore = PlayerPrefs.GetInt("bestScore");
        if (PlayerManager.playerScore > lastBestScore)
        {
            PlayerPrefs.SetInt("bestScore", PlayerManager.playerScore);
        }
    }

    /// <summary>
    /// Animate buttons on touch
    /// </summary>
    /// <param name="btn"></param>
    /// <returns></returns>
    private IEnumerator AnimateButton(GameObject btn)
    {
        canTap = false;
        var startingScale = btn.transform.localScale;
        var destinationScale = startingScale * 1.1f;

        var t = 0.0f;
        while (t <= 1.0f)
        {
            t += Time.deltaTime * buttonAnimationSpeed;
            btn.transform.localScale = new Vector3(Mathf.SmoothStep(startingScale.x, destinationScale.x, t),
                btn.transform.localScale.y,
                Mathf.SmoothStep(startingScale.z, destinationScale.z, t));
            yield return 0;
        }

        var r = 0.0f;
        if (btn.transform.localScale.x >= destinationScale.x)
        {
            while (r <= 1.0f)
            {
                r += Time.deltaTime * buttonAnimationSpeed;
                btn.transform.localScale = new Vector3(Mathf.SmoothStep(destinationScale.x, startingScale.x, r),
                    btn.transform.localScale.y,
                    Mathf.SmoothStep(destinationScale.z, startingScale.z, r));
                yield return 0;
            }
        }

        if (r >= 1)
        {
            canTap = true;
        }
    }
}