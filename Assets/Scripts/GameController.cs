﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Main GameController Class.
/// It supports two different modes: "Escape" & "Survival"
/// in escape mode, user must escape through various mazes.
/// in survival mode, user must avoid enemy balls reaching the bottom of the screen.
///  
/// This class clones the maze and enemy ball objects in the game.
/// It also manages the difficulty steep of the game, by increasing the movement speed of the elements.
/// </summary>
[DefaultExecutionOrder(-1000)] // start first
public class GameController : MonoBehaviour
{
    [SerializeField]
    private GameStateObject gameState;

    //AudioClips
    public AudioClip levelAdvanceSfx;
    public AudioClip gameoverSfx;

    //maze types
    public GameObject[] maze; //Array of all available mazes
    public GameObject enemyBall; //reference to the only enemyball object

    //Game finish variables
    public GameObject gameOverPlane; //reference to gameover plane (activates when we hit a maze)
    public GameObject mainBackground; //reference to the main background object (to modify its color)
    public GameObject player; //Reference to main ball object

    [Header("Settings")]
    public GameMode gameMode = GameMode.Escape; //escape by default
    public float levelJump = 10.0f; //increase the level every N seconds

    private Vector3 startPoint; //starting point of the clones object
    
    private static GameController instance;
    
    public static GameStateObject GameState => instance.gameState;

    /// <summary>
    /// Init everything here
    /// </summary>
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Debug.Log("Had to cleanup some leftovers");
            Destroy(instance.gameObject);
        }
        
        instance = this;

        gameOverPlane.SetActive(false); //hide the gameover plane
        mainBackground.GetComponent<Renderer>().material.color = new Color(1, 1, 1); //set the background color to default
        gameMode = (GameMode) PlayerPrefs.GetInt("GameMode"); //check game mode
        
        gameState = ScriptableObject.CreateInstance<GameStateObject>();
        gameState.hideFlags = HideFlags.HideAndDontSave;
        gameState.Reset(gameMode);
    }

    private void Update()
    {
        //If we have lost the set
        if (gameState.gameOver)
        {
            if (!gameState.gameOverFlag)
            {
                ProcessGameover();
            }

            return;
        }

        //Escape or Survival modes ?
        if (gameMode == 0)
        {
            //if we are allowed to spawn a maze
            if (gameState.createMaze)
            {
                CloneMaze();
            }
        }
        else
        {
            //if we are allowed to spawn enemyBall
            if (gameState.createEnemyBall)
            {
                CloneEnemyBall();
            }
        }

        //if the game is not yet finished, make it harder and harder by increasing the objects movement speed
        ModifyLevelDifficulty();
    }

    /// <summary>
    /// Clone Maze item based on a simple chance factor
    /// </summary>
    private void CloneMaze()
    {
        gameState.createMaze = false;
        startPoint = new Vector3(Random.Range(-1.0f, 1.0f), 0.5f, 7);
        Instantiate(maze[Random.Range(0, maze.Length)], startPoint, Quaternion.Euler(new Vector3(0, 0, 0)));
        StartCoroutine(ReactiveMazeCreation());
    }

    /// <summary>
    /// Clone a new enemyball object and let them have random sizes
    /// </summary>
    private void CloneEnemyBall()
    {
        gameState.createEnemyBall = false;
        startPoint = new Vector3(Random.Range(-3.2f, 3.2f), 0.5f, 7);
        var eb = Instantiate(enemyBall, startPoint, Quaternion.Euler(new Vector3(0, 0, 0)));
        eb.name = "enemyBall";
        var scaleModifier = Random.Range(-0.4f, 0.1f);
        eb.transform.localScale = new Vector3(eb.transform.localScale.x + scaleModifier,
            eb.transform.localScale.y,
            eb.transform.localScale.z + scaleModifier);
        StartCoroutine(ReactiveEnemyBallCreation());
    }

    /// <summary>
    /// enable this controller to be able to clone maze objects again
    /// </summary>
    /// <returns></returns>
    private IEnumerator ReactiveMazeCreation()
    {
        yield return new WaitForSeconds(gameState.cloneInterval);
        gameState.createMaze = true;
    }


    /// <summary>
    /// enable this controller to be able to clone enemyball objects again
    /// </summary>
    /// <returns></returns>
    private IEnumerator ReactiveEnemyBallCreation()
    {
        yield return new WaitForSeconds(0.35f);
        gameState.createEnemyBall = true;
    }

    /// <summary>
    /// Here can increase gameSpeed and decrease itemCloneInterval values to make the game harder.
    /// </summary>
    private void ModifyLevelDifficulty()
    {
        gameState.levelPassedTime = Time.timeSinceLevelLoad;
        if (gameState.levelPassedTime > gameState.levelStartTime + levelJump)
        {
            //increase level difficulty (but limit it to a maximum level of 10)
            if (gameState.currentLevel < 10)
            {
                gameState.currentLevel += 1;

                //let the player know what happened to him/her
                this.PlaySfx(levelAdvanceSfx);

                //increase difficulty by increasing movement speed
                gameState.moveSpeed += 0.6f;

                //clone items faster
                gameState.cloneInterval -= 0.18f; //very important!!!
                print("cloneInterval: " + gameState.cloneInterval);
                if (gameState.cloneInterval < 0.3f)
                {
                    gameState.cloneInterval = 0.3f;
                }

                gameState.levelStartTime += levelJump;

                //Background color correction (fade to red)
                var colorCorrection = gameState.currentLevel / 10.0f;
                //print("colorCorrection: " + colorCorrection);
                mainBackground.GetComponent<Renderer>().material.color = new Color(1,
                    1 - colorCorrection,
                    1 - colorCorrection);
            }
        }
    }

    /// <summary>
    /// Game Over routine
    /// </summary>
    private void ProcessGameover()
    {
        gameState.gameOverFlag = true;
        this.PlaySfx(gameoverSfx);

        //show gameover menu
        gameOverPlane.SetActive(true);

        PlayFabManager.Instance.SendPointsEvent(PlayerManager.playerScore, gameMode.ToString());
        PlayFabManager.Instance.SendSessionEvent(PlayerManager.sessionLength.Floor());
    }
}